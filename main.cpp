#include "windows_shell.h"

int main()
{
	WindowsShell ws;
	vector<string> input;
	//infinite loop
	while (true)
	{
		input = ws.getInput(); //getting the input

		if (input[0] == "pwd") //if the selected function is pwd
		{
			ws.getPwd();
		}
		else if (input.size() > 1) //for functions that require more then one input
		{
			if (input[0] == "cd") //if the selected function is cd
			{
				ws.cdFunc(input[1]);
			}
			else if (input[0] == "create") //if the selected function is create
			{
				string finalStr;
				for (int i = 1; i < input.size(); i++)
				{
					finalStr += input[i]; //getting all of the file name (w/ spaces)
				}
				ws.createFile(finalStr);
			}
		}
	}

	return 0;
}