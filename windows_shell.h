#pragma once
#include <Windows.h>
#include <string>
#include <iostream>
#include <vector>
#include "Helper.h"

using namespace std;

class WindowsShell
{
public:
	vector<string> getInput();
	void getPwd();
	void cdFunc(string dir);
	void createFile(string name);
};