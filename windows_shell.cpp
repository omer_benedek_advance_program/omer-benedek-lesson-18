#include "windows_shell.h"

//gets the function name from the user and other data
vector<string> WindowsShell::getInput()
{
	TCHAR input[MAX_PATH];
	Helper help;

	cout << ">> ";
	cin.getline(input, sizeof(input));
	
	return help.get_words(string(input));
}

//function that answers to the pwd function from the cmd
void WindowsShell::getPwd()
{
	TCHAR Buffer[MAX_PATH];
	DWORD rc = GetCurrentDirectory(MAX_PATH, Buffer);

	if (!rc)
	{
		cout << "Fail to preform pwd" << endl << "Error code: " << GetLastError() << endl;
	}

	cout << Buffer << endl;
}

//function that answers to the cd function from the cmd
void WindowsShell::cdFunc(string dir)
{
	LPCTSTR path = dir.c_str();
	DWORD rc = SetCurrentDirectory(path);

	if (!rc)
	{
		cout << "Fail to preform cd" << endl << "Error code: " << GetLastError() << endl;
	}
}

//function that answers to the create function from the cmd
void WindowsShell::createFile(string name)
{
	HANDLE hFile = CreateFile(name.c_str(), GENERIC_WRITE, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile == INVALID_HANDLE_VALUE)
	{
		cout << "Fail to preform create" << endl << "Error code: " << GetLastError() << endl;
	}

	CloseHandle(hFile);
}